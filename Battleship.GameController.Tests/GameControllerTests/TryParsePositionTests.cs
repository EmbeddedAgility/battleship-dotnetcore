﻿using Battleship.GameController.Contracts;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Battleship.GameController.Tests.GameControllerTests
{
    [TestClass]
    public class TryParsePositionTests
    {
        [TestMethod]
        public void ParseValidPosition()
        {
            Assert.IsTrue(GameController.TryParsePosition("A1", out Position position1));
            Assert.IsNotNull(position1);
        }

        [TestMethod]
        public void ParseInvalidPosition()
        {
            Assert.IsFalse(GameController.TryParsePosition("Z0", out Position position));
            Assert.IsNull(position);

            Assert.IsFalse(GameController.TryParsePosition("Z-1", out Position position2));
            Assert.IsNull(position2);
        }
    }
}
