﻿using Battleship.GameController.Contracts;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Battleship.GameController.Tests.GameControllerTests
{
    [TestClass]
    public class ShipPositionValidationTests
    {
        // run tests one by one
        [TestMethod]
        public void ShouldValidateShipPositionAgainstItself()
        {
            var ship = new Ship();

            Assert.IsTrue(ship.TryAddPosition("A1", out _));
            Assert.IsFalse(ship.TryAddPosition("A1", out _));
        }

        [TestMethod]
        public void CannotPlaceShipOutsideOfBoard()
        {
            var ship = new Ship();
            Assert.IsFalse(ship.TryAddPosition("C25", out _));
        }

        [TestMethod]
        public void ShipsCannotOverlap()
        {
            var ship1 = new Ship();
            ship1.TryAddPosition("A1", out _);
            ship1.TryAddPosition("A2", out _);

            var ship2 = new Ship();

            Assert.IsFalse(ship2.TryAddPosition("A1", out _));
        }

        [TestMethod]
        public void GivenOnlyFirstShipPosition_ShipCannotBeDiagonal()
        {
            var ship = new Ship();

            ship.TryAddPosition("A1", out _);
            Assert.IsFalse(ship.TryAddPosition("B2", out _));
        }

        [TestMethod]
        public void GivenVerticalShipPosition_NewPositionMustBeContiguous()
        {
            var ship = new Ship();
            ship.TryAddPosition("A1", out _);
            ship.TryAddPosition("A2", out _);

            Assert.IsFalse(ship.TryAddPosition("B1", out _));
            Assert.IsFalse(ship.TryAddPosition("A4", out _));
            Assert.IsTrue(ship.TryAddPosition("A3", out _));
            Assert.IsTrue(ship.TryAddPosition("A4", out _));
        }

        [TestMethod]
        public void GivenHorizontalShipPosition_NewPositionMustBeContiguous()
        {
            var ship = new Ship();
            ship.TryAddPosition("A1", out _);
            ship.TryAddPosition("B1", out _);

            Assert.IsTrue(ship.TryAddPosition("C1", out _));
            Assert.IsFalse(ship.TryAddPosition("A2", out _));
        }
    }
}
