﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Battleship.GameController.Contracts
{
    public static class Board
    {
        public const int Rows = 8;
        public const int Columns = 8;

        private static readonly Ship[,] SCells = new Ship[Columns, Rows];

        public static bool TryAddShip(Position position, Ship ship)
        {
            if (!IsWithinBoard(position))
            {
                return false;
            }

            if (IsPositionTaken(position))
            {
                return false;
            }

            SCells[(int)position.Column, position.Row - 1] = ship;

            return true;
        }

        private static bool IsPositionTaken(Position position)
        {
            return SCells[(int) position.Column, position.Row - 1] != null;
        }

        private static bool IsWithinBoard(Position position)
        {
            return position.Row - 1 < Rows && (int)position.Column < Columns;
        }
    }
}
