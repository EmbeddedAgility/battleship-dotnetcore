﻿using System.Linq;

namespace Battleship.GameController.Contracts
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// The ship.
    /// </summary>
    public class Ship
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="Ship"/> class.
        /// </summary>
        public Ship()
        {
            Positions = new List<Position>();
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the positions.
        /// </summary>
        public List<Position> Positions { get; set; }

        /// <summary>
        /// The color of the ship
        /// </summary>
        public ConsoleColor Color { get; set; }

        /// <summary>
        /// Gets or sets the size.
        /// </summary>
        public int Size { get; set; }

        #endregion

        #region Public Methods and Operators

        public bool TryAddPosition(string input, out string error)
        {
            error = string.Empty;

            try
            {
                var letter = (Letters)Enum.Parse(typeof(Letters), input.ToUpper()[..1]);
                var number = int.Parse(input[1..]);
                var position = new Position { Column = letter, Row = number };

                if (!CanAddPositionToShip(position))
                {
                    error = "Ship must be contiguous in row or column";
                    return false;
                }

                if (!Board.TryAddShip(position, this))
                {
                    error = "Cannot add ship in given position";
                    return false;
                }
                Positions.Add(position);
            }
            catch
            {
                error = "Invalid position.";
                return false;
            }

            return true;
        }

        public bool IsAlive()
        {
            foreach (var item in this.Positions)
            {
                if (!item.IsHit)
                {
                    return true;
                }
            }

            return false;
        }

        private bool CanAddPositionToShip(Position position)
        {
            if (Positions.Count == 0) return true;
            if (Positions.Count == 1)
            {
                var vDist = Math.Abs(Positions[0].Row - position.Row);
                var hDist = Math.Abs(Positions[0].Column - position.Column);

                return !(vDist > 1 || hDist > 1) && (vDist == 0 || hDist == 0);
            }

            if (Positions[0].Row - Positions[1].Row == 0) // is horizontal
            {
                var minCol = Positions.Min(p => p.Column);
                var maxCol = Positions.Max(p => p.Column);

                if (position.Row != Positions[0].Row)
                    return false;

                if (Math.Abs(position.Column - minCol) == 1 || Math.Abs(position.Column - maxCol) == 1)
                    return true;
            }
            else // is vertical
            {
                var minRow = Positions.Min(p => p.Row);
                var maxRow = Positions.Max(p => p.Row);

                if (position.Column != Positions[0].Column)
                    return false;

                if (Math.Abs(position.Row - minRow) == 1 || Math.Abs(position.Row - maxRow) == 1)
                    return true;
            }

            return false;
        }

        #endregion
    }
}