﻿using Battleship.GameController.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Battleship.Ascii
{
    public static class EnemyFleetGenerator
    {
        public static IEnumerable<Ship> Generate()
        {
            Random random = new Random();
            var nextRandom = random.Next(5);

            switch (nextRandom)
            {
                case 4:
                    return FleeStrategy4();
                case 3:
                    return FleeStrategy3();
                case 2:
                    return FleeStrategy2();
                default:
                    return FleeStrategy1();
            }
        }

        private static IEnumerable<Ship> FleeStrategy1()
        {
            var enemyFleet = GameController.GameController.InitializeShips().ToList();

            enemyFleet[0].Positions.Add(new Position { Column = Letters.B, Row = 4 });
            enemyFleet[0].Positions.Add(new Position { Column = Letters.B, Row = 5 });
            enemyFleet[0].Positions.Add(new Position { Column = Letters.B, Row = 6 });
            enemyFleet[0].Positions.Add(new Position { Column = Letters.B, Row = 7 });
            enemyFleet[0].Positions.Add(new Position { Column = Letters.B, Row = 8 });

            enemyFleet[1].Positions.Add(new Position { Column = Letters.E, Row = 6 });
            enemyFleet[1].Positions.Add(new Position { Column = Letters.E, Row = 7 });
            enemyFleet[1].Positions.Add(new Position { Column = Letters.E, Row = 8 });
            enemyFleet[1].Positions.Add(new Position { Column = Letters.E, Row = 9 });

            enemyFleet[2].Positions.Add(new Position { Column = Letters.A, Row = 3 });
            enemyFleet[2].Positions.Add(new Position { Column = Letters.B, Row = 3 });
            enemyFleet[2].Positions.Add(new Position { Column = Letters.C, Row = 3 });

            enemyFleet[3].Positions.Add(new Position { Column = Letters.F, Row = 8 });
            enemyFleet[3].Positions.Add(new Position { Column = Letters.G, Row = 8 });
            enemyFleet[3].Positions.Add(new Position { Column = Letters.H, Row = 8 });

            enemyFleet[4].Positions.Add(new Position { Column = Letters.C, Row = 5 });
            enemyFleet[4].Positions.Add(new Position { Column = Letters.C, Row = 6 });

            return enemyFleet;
        }

        private static IEnumerable<Ship> FleeStrategy2()
        {
            var enemyFleet = GameController.GameController.InitializeShips().ToList();

            enemyFleet[0].Positions.Add(new Position { Column = Letters.A, Row = 1 });
            enemyFleet[0].Positions.Add(new Position { Column = Letters.B, Row = 1 });
            enemyFleet[0].Positions.Add(new Position { Column = Letters.C, Row = 1 });
            enemyFleet[0].Positions.Add(new Position { Column = Letters.D, Row = 1 });
            enemyFleet[0].Positions.Add(new Position { Column = Letters.E, Row = 1 });

            enemyFleet[1].Positions.Add(new Position { Column = Letters.C, Row = 6 });
            enemyFleet[1].Positions.Add(new Position { Column = Letters.C, Row = 7 });
            enemyFleet[1].Positions.Add(new Position { Column = Letters.C, Row = 8 });
            enemyFleet[1].Positions.Add(new Position { Column = Letters.C, Row = 9 });

            enemyFleet[2].Positions.Add(new Position { Column = Letters.A, Row = 4 });
            enemyFleet[2].Positions.Add(new Position { Column = Letters.B, Row = 4 });
            enemyFleet[2].Positions.Add(new Position { Column = Letters.C, Row = 4 });

            enemyFleet[3].Positions.Add(new Position { Column = Letters.F, Row = 5 });
            enemyFleet[3].Positions.Add(new Position { Column = Letters.G, Row = 5 });
            enemyFleet[3].Positions.Add(new Position { Column = Letters.H, Row = 5 });

            enemyFleet[4].Positions.Add(new Position { Column = Letters.D, Row = 5 });
            enemyFleet[4].Positions.Add(new Position { Column = Letters.D, Row = 6 });

            return enemyFleet;
        }

        private static IEnumerable<Ship> FleeStrategy3()
        {
            var enemyFleet = GameController.GameController.InitializeShips().ToList();

            enemyFleet[0].Positions.Add(new Position { Column = Letters.A, Row = 4 });
            enemyFleet[0].Positions.Add(new Position { Column = Letters.B, Row = 4 });
            enemyFleet[0].Positions.Add(new Position { Column = Letters.C, Row = 4 });
            enemyFleet[0].Positions.Add(new Position { Column = Letters.D, Row = 4 });
            enemyFleet[0].Positions.Add(new Position { Column = Letters.E, Row = 4 });

            enemyFleet[1].Positions.Add(new Position { Column = Letters.C, Row = 4 });
            enemyFleet[1].Positions.Add(new Position { Column = Letters.C, Row = 3 });
            enemyFleet[1].Positions.Add(new Position { Column = Letters.C, Row = 2 });
            enemyFleet[1].Positions.Add(new Position { Column = Letters.C, Row = 1 });

            enemyFleet[2].Positions.Add(new Position { Column = Letters.A, Row = 5 });
            enemyFleet[2].Positions.Add(new Position { Column = Letters.B, Row = 5 });
            enemyFleet[2].Positions.Add(new Position { Column = Letters.C, Row = 5 });

            enemyFleet[3].Positions.Add(new Position { Column = Letters.F, Row = 6 });
            enemyFleet[3].Positions.Add(new Position { Column = Letters.G, Row = 6 });
            enemyFleet[3].Positions.Add(new Position { Column = Letters.H, Row = 6 });

            enemyFleet[4].Positions.Add(new Position { Column = Letters.D, Row = 7 });
            enemyFleet[4].Positions.Add(new Position { Column = Letters.D, Row = 8 });

            return enemyFleet;
        }

        private static IEnumerable<Ship> FleeStrategy4()
        {
            var enemyFleet = GameController.GameController.InitializeShips().ToList();

            enemyFleet[0].Positions.Add(new Position { Column = Letters.A, Row = 2 });
            enemyFleet[0].Positions.Add(new Position { Column = Letters.B, Row = 2 });
            enemyFleet[0].Positions.Add(new Position { Column = Letters.C, Row = 2 });
            enemyFleet[0].Positions.Add(new Position { Column = Letters.D, Row = 2 });
            enemyFleet[0].Positions.Add(new Position { Column = Letters.E, Row = 2 });

            enemyFleet[1].Positions.Add(new Position { Column = Letters.F, Row = 4 });
            enemyFleet[1].Positions.Add(new Position { Column = Letters.F, Row = 3 });
            enemyFleet[1].Positions.Add(new Position { Column = Letters.F, Row = 2 });
            enemyFleet[1].Positions.Add(new Position { Column = Letters.F, Row = 1 });

            enemyFleet[2].Positions.Add(new Position { Column = Letters.A, Row = 5 });
            enemyFleet[2].Positions.Add(new Position { Column = Letters.B, Row = 5 });
            enemyFleet[2].Positions.Add(new Position { Column = Letters.C, Row = 5 });

            enemyFleet[3].Positions.Add(new Position { Column = Letters.F, Row = 8 });
            enemyFleet[3].Positions.Add(new Position { Column = Letters.G, Row = 8 });
            enemyFleet[3].Positions.Add(new Position { Column = Letters.H, Row = 8 });

            enemyFleet[4].Positions.Add(new Position { Column = Letters.A, Row = 1 });
            enemyFleet[4].Positions.Add(new Position { Column = Letters.B, Row = 1 });

            return enemyFleet;
        }
    }
}
