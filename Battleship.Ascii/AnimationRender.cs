﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Threading;

namespace Battleship.Ascii
{
    public class AnimationRender
    {
        public  void RenderWinner()
        {
            Console.ForegroundColor = ConsoleColor.Green;
            RenderImage("assets/winner.png");
            Console.ForegroundColor = ConsoleColor.White;
        }

        public  void RenderLoser()
        {
            Console.ForegroundColor = ConsoleColor.Red;
            RenderImage("assets/loser.png");
            Console.ForegroundColor = ConsoleColor.White;
        }

        private void RenderImage(string imageFile)
        {
            Image Picture = Image.FromFile(imageFile);
            //Console.SetBufferSize(Picture.Width * 0x3, Picture.Height * 0x3);

            FrameDimension Dimension = new FrameDimension(Picture.FrameDimensionsList[0x0]);
            int FrameCount = Picture.GetFrameCount(Dimension);
            int Left = Console.WindowLeft, Top = Console.WindowTop;
            char[] Chars = { '#', '#', '@', '%', '=', '+', '*', ':', '-', '.', ' ' };
            Picture.SelectActiveFrame(Dimension, 0x0);
            for (int i = 0x0; i < Picture.Height; i++)
            {
                for (int x = 0x0; x < Picture.Width; x++)
                {
                    Color Color = ((Bitmap)Picture).GetPixel(x, i);
                    int Gray = (Color.R + Color.G + Color.B) / 0x3;
                    int Index = (Gray * (Chars.Length - 0x1)) / 0xFF;
                    Console.Write(Chars[Index]);
                }
                Console.Write('\n');
                Thread.Sleep(50);
            }
            Console.SetCursorPosition(Left, Top);
        }
    }
}
