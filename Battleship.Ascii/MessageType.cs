﻿namespace Battleship.Ascii
{
    public enum MessageType
    {
        Negative,
        Positive,
        Information
    }

}
