﻿
using System.Runtime.InteropServices;

namespace Battleship.Ascii
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Battleship.GameController;
    using Battleship.GameController.Contracts;

    public class Program
    {
        private static List<Ship> myFleet;

        private static List<Ship> enemyFleet;

        private static bool computerLastShotWasAHit { get; set; }
        private static Position computerLastShotPosition { get; set; } = new Position();

        private static HashSet<Position> PreviousShots { get; set; } = new HashSet<Position>();

        public static MessageType MessageType { get; set; } = MessageType.Information;

        public static bool IsVictory { get; set; }

        static void Main()
        {
            RenderConsoleMessage("                                     |__");
            RenderConsoleMessage(@"                                     |\/");
            RenderConsoleMessage("                                     ---");
            RenderConsoleMessage("                                     / | [");
            RenderConsoleMessage("                              !      | |||");
            RenderConsoleMessage("                            _/|     _/|-++'");
            RenderConsoleMessage("                        +  +--|    |--|--|_ |-");
            RenderConsoleMessage(@"                     { /|__|  |/\__|  |--- |||__/");
            RenderConsoleMessage(@"                    +---------------___[}-_===_.'____                 /\");
            RenderConsoleMessage(@"                ____`-' ||___-{]_| _[}-  |     |_[___\==--            \/   _");
            RenderConsoleMessage(@" __..._____--==/___]_|__|_____________________________[___\==--____,------' .7");
            RenderConsoleMessage(@"|                        Welcome to Battleship                         BB-61/");
            RenderConsoleMessage(@" \_________________________________________________________________________|");
            RenderConsoleMessage();

            InitializeGame();

            StartGame();
        }

        private static void StartGame()
        {
            Console.Clear();
            RenderConsoleMessage("                  __");
            RenderConsoleMessage(@"                 /  \");
            RenderConsoleMessage("           .-.  |    |");
            RenderConsoleMessage(@"   *    _.-'  \  \__/");
            RenderConsoleMessage(@"    \.-'       \");
            RenderConsoleMessage("   /          _/");
            RenderConsoleMessage(@"  |      _  /""");
            RenderConsoleMessage(@"  |     /_\'");
            RenderConsoleMessage(@"   \    \_/");
            RenderConsoleMessage(@"    """"""""");

            do
            {
                MessageType = MessageType.Information;
                RenderConsoleMessage();
                RenderEnemyFleet();
                RenderConsoleMessage();
                RenderConsoleMessage("Player, it's your turn");

                bool isValidCoordinates;
                Position position;

                do
                {
                    RenderConsoleMessage("Enter coordinates for your shot :");
                    isValidCoordinates = GameController.TryParsePosition(Console.ReadLine(), out position);
                    if (!isValidCoordinates)
                    {
                        MessageType = MessageType.Negative;
                        RenderConsoleMessage("Invalid Coordinates.");
                        MessageType = MessageType.Information;
                    }

                } while (!isValidCoordinates);
                
                var isHit = GameController.CheckIsHit(enemyFleet, position);
                if (isHit)
                {
                    Console.Beep();

                    MessageType = MessageType.Positive;
                    RenderConsoleMessage(@"                \         .  ./");
                    RenderConsoleMessage(@"              \      .:"";'.:..""   /");
                    RenderConsoleMessage(@"                  (M^^.^~~:.'"").");
                    RenderConsoleMessage(@"            -   (/  .    . . \ \)  -");
                    RenderConsoleMessage(@"               ((| :. ~ ^  :. .|))");
                    RenderConsoleMessage(@"            -   (\- |  \ /  |  /)  -");
                    RenderConsoleMessage(@"                 -\  \     /  /-");
                    RenderConsoleMessage(@"                   \  \   /  /");
                }

                MessageType = MessageType.Positive;
                if (isHit)
                {
                    MessageType = MessageType.Positive;
                    RenderConsoleMessage("Yeah ! Nice hit !");
                }
                else
                {
                    MessageType = MessageType.Negative;
                    RenderConsoleMessage("Miss");
                }

                RenderConsoleMessage();

                do
                {
                    //position = computerLastShotWasAHit ? GetNearbyPosition() : GetRandomPosition();
                    position = GetRandomPosition();
                } while (PreviousShots.Contains(position));
                PreviousShots.Add(position);

                isHit = GameController.CheckIsHit(myFleet, position);

                computerLastShotPosition.Column = position.Column;
                computerLastShotPosition.Row = position.Row;
                computerLastShotWasAHit = isHit;

                MessageType = MessageType.Negative;

                if (isHit)
                {
                    MessageType = MessageType.Negative;
                    RenderConsoleMessage($"\nComputer shot in {position.Column}{position.Row} and has hit your ship !");
                }
                else
                {
                    MessageType = MessageType.Positive;
                    RenderConsoleMessage($"\nComputer shot in {position.Column}{position.Row} and miss.");
                }

                if (isHit)
                {
                    Console.Beep();
                    RenderConsoleMessage();

                    MessageType = MessageType.Negative;
                    RenderConsoleMessage(@"                \         .  ./");
                    RenderConsoleMessage(@"              \      .:"";'.:..""   /");
                    RenderConsoleMessage(@"                  (M^^.^~~:.'"").");
                    RenderConsoleMessage(@"            -   (/  .    . . \ \)  -");
                    RenderConsoleMessage(@"               ((| :. ~ ^  :. .|))");
                    RenderConsoleMessage(@"            -   (\- |  \ /  |  /)  -");
                    RenderConsoleMessage(@"                 -\  \     /  /-");
                    RenderConsoleMessage(@"                   \  \   /  /");

                }
            }
            while (!IsGameOver());

            AnimationRender animationRender = new AnimationRender();

            if (IsVictory)
            {
                MusicPlayer.PlayWinnerMusic();
                animationRender.RenderWinner();
            }
            else
            {
                MusicPlayer.PlayLoserMusic();
                animationRender.RenderLoser();
            }
        }

        private static bool IsGameOver()
        {
            var isMyFleetAlive = myFleet.Any(x => x.IsAlive());
            IsVictory = isMyFleetAlive;

            var isEnemyFleetAlive = enemyFleet.Any(x => x.IsAlive());

            return !isMyFleetAlive || !isEnemyFleetAlive;
        }

        private static Position GetRandomPosition()
        {
            int rows = 8;
            int lines = 8;
            var random = new Random();
            var letter = (Letters)random.Next(lines);
            var number = random.Next(rows);
            var position = new Position(letter, number);
            return position;
        }

        //private static Position GetNearbyPosition()
        //{
        //    List<Position> possiblePositions = new List<Position>();

        //    if (computerLastShotPosition.Row > 1)
        //    {
        //        possiblePositions.Add(new Position(computerLastShotPosition.Column, computerLastShotPosition.Row + 1));
        //    }
        //    if (computerLastShotPosition.Row > 1)
        //    {
        //        possiblePositions.Add(new Position(computerLastShotPosition.Column, computerLastShotPosition.Row + 1));
        //    }
        //}

        private static void InitializeGame()
        {
            InitializeMyFleet();

            InitializeEnemyFleet();
        }

        private static void InitializeMyFleet()
        {
            myFleet = GameController.InitializeShips().ToList();

            RenderConsoleMessage("Please position your fleet (Game board size is from A to H and 1 to 8) :");

            foreach (var ship in myFleet)
            {
                RenderConsoleMessage();
                RenderConsoleMessage(string.Format("Please enter the positions for the {0} (size: {1})", ship.Name, ship.Size));
                for (var i = 1; i <= ship.Size; i++)
                {
                    var positionAdded = false;
                    do
                    {
                        RenderConsoleMessage(string.Format("Enter position {0} of {1} (i.e A3):", i, ship.Size));
                        positionAdded = ship.TryAddPosition(Console.ReadLine(), out var error);

                        if (positionAdded) continue;

                        var originalMsgType = MessageType;
                        MessageType = MessageType.Negative;
                        RenderConsoleMessage(error);
                        MessageType = originalMsgType;
                    } while (!positionAdded);
                }
            }
        }

        private static void InitializeEnemyFleet()
        {
            enemyFleet = EnemyFleetGenerator.Generate().ToList();
        }

        private static void RenderConsoleMessage(string message = "")
        {
            switch (MessageType)
            {
                case MessageType.Positive:
                    Console.ForegroundColor = ConsoleColor.Green;
                    break;
                case MessageType.Negative:
                    Console.ForegroundColor = ConsoleColor.Red;
                    break;
                default:
                    Console.ForegroundColor = ConsoleColor.White;
                    break;
            }

            Console.WriteLine(message);
        }

        private static void RenderEnemyFleet()
        {
            MessageType = MessageType.Information;
            RenderConsoleMessage("Enemy fleet");
            RenderConsoleMessage("----------------");

            RenderConsoleMessage("Ships that sunk:");
            foreach (var boat in enemyFleet.Where(x => !x.IsAlive()))
            {
                MessageType = MessageType.Positive;
                RenderConsoleMessage($"{boat.Name}");
            }

            MessageType = MessageType.Information;
            RenderConsoleMessage("Ships leftover:");
            foreach (var boat in enemyFleet.Where(x => x.IsAlive()))
            {
                MessageType = MessageType.Negative;
                RenderConsoleMessage($"{boat.Name}");
            }

            MessageType = MessageType.Information;

            RenderConsoleMessage("----------------");
            RenderConsoleMessage("----------------");
            RenderConsoleMessage();
        }
    }
}
