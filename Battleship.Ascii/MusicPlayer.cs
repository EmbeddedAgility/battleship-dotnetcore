﻿using System;
using System.Threading;

namespace Battleship.Ascii
{
    public static class MusicPlayer
    {
        public static void PlayWinnerMusic()
        {
            new Thread(() =>
            {
                Beep(659, 125); Beep(659, 125); Thread.Sleep(125); Beep(659, 125); Thread.Sleep(167); Beep(523, 125); Beep(659, 125); Thread.Sleep(125); Beep(784, 125); Thread.Sleep(375); Beep(392, 125);
            }).Start();
        }

        public static void PlayLoserMusic()
        {
            new Thread(() =>
            {
                Beep(1320, 500); Beep(990, 250); Beep(1056, 250); Beep(1188, 250); Beep(1320, 125); Beep(1188, 125); Beep(1056, 250); Beep(990, 250); Beep(880, 500);
            }).Start();
        }

        private static void Beep(int a, int b)
        {
            Console.Beep(a, b);
        }
    }
}
